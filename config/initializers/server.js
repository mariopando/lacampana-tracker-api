// config/initializers/server.js

var express = require('express');
var path = require('path');

// Local dependencies
var config = require('nconf');
var userhome = require('userhome');

//ini parser
var ini = require('node-ini');
var environment = require('../../app/models/environment.js');

// create the express app
// configure middlewares
var bodyParser = require('body-parser');
var morgan = require('morgan');
var logger = require('winston');
var cors = require('cors');
var app;

var start =  function(cb) {
    'use strict';
    // Configure express
    app = express();

    app.use(morgan('common'));
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json({type: '*/*'}));
    app.use(cors());

    ini.parse(userhome()+config.get('RISE_VISION_DISPLAY_FILE'), function(err,data){
        if(err){
            config.set('DISPLAYID', false);
            logger.info('[ERROR] NOT FOUND INI RISE VISION DISPLAY FILE')
        }
        else {
            //update display id value
            environment.findOneAndUpdate({ name:'DISPLAYID' }, { name:'DISPLAYID', value:data.displayid }, { upsert: true}, function(err, doc){
                if (err) return res.send(500, { error: err });
            });

            config.set('DISPLAYID', data.displayid);
            logger.info('[RISE VISION] Display id:'+config.get('DISPLAYID'))
        }

        logger.info('[SERVER] Initializing routes');
        require('../../app/routes/index')(app);
    });



    app.use(express.static(path.join(__dirname, 'public')));

    // Error handler
    app.use(function errorHandler(err, req, res, next) {
        if (res.headersSent) {
            return next(err);
        }
        res.status(500);
        //res.render('error', { error: err });
        res.json({ error: err });
    });

    app.listen(config.get('NODE_PORT'));
    logger.info('[SERVER] Listening on port ' + config.get('NODE_PORT'));

    if (cb) {
        return cb();
    }
};

module.exports = start;
