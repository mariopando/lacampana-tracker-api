// config/initializers/database.js
var mongoose = require('mongoose');
var logger = require('winston');

module.exports = function(cb) {
    'use strict';
    // Initialize the component here then call the callback
    // More logic
    //
    // Return the call back
    //cb();console.log(cb)

    mongoose.connect('mongodb://localhost:27017/lacampana', function(err) {
        if(err) {
            logger.info('[DATABASE] database connection error', err);

        } else {
            logger.info('[DATABASE] connection successful');
            return cb();
        }
    });
};