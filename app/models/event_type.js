// Event Types Model
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

/*
* Values
* [APP_TOUCH_EVENT, APP_FORM_EVENT, APP_CAMERA_EVENT]
**/
var type_eventSchema = new Schema({
        name:  {
            type: String ,
            index: true
        },
        payload:  {
            type: String
        }
    },{
        collection:'event_type'
    });
module.exports = mongoose.model('event_type', type_eventSchema);