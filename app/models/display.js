// Display Entity
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var eventSchema = new Schema({
    name:  { type: String, required: true },
    display_id:{ type: String, required: true },
    address:  { type: String },
    created_at: Date,
    updated_at: Date
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'}
});

module.exports = mongoose.model('display', eventSchema);