// Display Entity
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var eventSchema = new Schema({
    name:  { type: String, required: true },
    value:{ type: String, required: true },
    created_at: Date,
    updated_at: Date
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'}
});

module.exports = mongoose.model('environment', eventSchema);