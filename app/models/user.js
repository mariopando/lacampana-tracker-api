// User Model
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var type_eventSchema = new Schema({
    name:{
        type: String
    },
    username:{
        type: String,
        index: true
    },
    organization_position:{
        type: String
    },
    password:{
        type: String
    },
    admin:{
        type: Boolean
    },
    api:{
        type: Boolean
    }
},{
    collection:'user'
});
module.exports = mongoose.model('user', type_eventSchema);