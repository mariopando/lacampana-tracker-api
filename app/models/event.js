// Event Model
var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;
var config = require('nconf');

var eventSchema = new Schema({
    type_id:  { type: String, required: true },
    type_name:{ type: String },
    payload:  { type: String },
    display_id:  { type: String, default: config.get('DISPLAYID') },
    version:  { type: Number, default: 1.0 },
    created_at: Date,
    updated_at: Date
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'}
});

module.exports = mongoose.model('event', eventSchema);