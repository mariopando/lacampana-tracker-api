/**
 * Created by desarrollo on 8/24/16.
 */
var express = require('express');

var User = require('../models/user.js');
var jwt  = require('jsonwebtoken');
var config = require('nconf');
var app;

module.exports = function(router){
    router.route('/')
        .post(function(req, res, next) {
            // find the user
            User.findOne({
                username: req.body.username
            }, function(err, user) {

                if (err) throw err;

                if (!user) {
                    res.json({ status: false, message: 'Authentication failed. User not found.' });
                } else if (user) {

                    // check if password matches
                    if ( user.password != req.body.password ) {
                        res.json({ status: false, message: 'Authentication failed or User without API permissions.' });
                    } else {

                        // if user is found and password is right
                        // create a token
                        app = express();

                        var token = jwt.sign(user, config.get('SESSION_SECRET'), {
                            expiresIn: 60*60*24 // expires in 24 hours
                        });

                        // return the information including token as JSON
                        res.json({
                            status: true,
                            payload: { name: user.name, position: user.organization_position },
                            token: token
                        });
                    }

                }

            });
    });
};