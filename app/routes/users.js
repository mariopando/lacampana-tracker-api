// app/routes/users.js
var User = require('../models/user.js');
var jwt  = require('jsonwebtoken');
var config = require('nconf');

module.exports = function(router) {
    'use strict';
    router.route('/reset_admin')
        .post(function(req, res, next) {
            // create a sample user
            var nick = new User({
                name: 'Mario Pando',
                username: 'mpando',
                password: 'gestionweb00',
                admin: true,
                api: true
            });

            // save the sample user
            nick.save(function(err) {
                if (err) throw err;

                console.log('User saved successfully');
                res.json({ success: true, msg: 'User saved successfully' });
            });
        });

    /**
     *  Path: /users/:user_id
     **/
    router.route('/:userId')
        .get(function(req, res, next) {
            res.json({ success: true, msg: 'get user ' + req.params.userId });
        })
        .put(function(req, res, next) {
            // Update user
        })
        .patch(function(req, res,next) {
            // Patch
        })
        .delete(function(req, res, next) {
            // Delete record
        });

    /**
     *  Path: /
     **/
    router.route('/')
        .get(function(req, res, next) {
            User.find({}, function(err, users) {
                if (err){
                    res.json({ status: 0, payload: err });
                    return next(err);
                }
                else{
                    res.json({ status: 1, payload: users });
                }
            });
        }).post(function(req, res, next) {
        // Create new user
    });

    /**
     * Token Verify
     */
    router.use(function(req, res, next) {

        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, config.get('SESSION_SECRET'), function(err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }
    });
};