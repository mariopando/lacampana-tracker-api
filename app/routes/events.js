// Events API route
var event = require('../models/event.js');
var jwt  = require('jsonwebtoken');
var config = require('nconf');


module.exports = function(router) {
    'use strict';

    // events
    router.route('/')
        .get(function(req, res, next) {
            event.find({}, function(err, event) {
                if (err){
                    res.json({ status: 0, payload: err });
                    return next(err);
                }
                else{
                    //if(req.params.start_date && req.params.end_date){
                    //
                    //}
                    res.json({ status: 1, payload: event });
                }
            }).sort({created_at: -1});
        })
        .post(function(req, res, next) {
            // Create new event
            event.create(req.body, function (err, post) {
                if (err){
                    res.json({ status: 0, payload: err });
                    return next(err);
                }
                else{
                    res.json({ status: 1, payload: post });
                }
            });
        });

    /**
     * Token Verify
     */
    router.use(function(req, res, next) {

        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, config.get('SESSION_SECRET'), function(err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }
    });
};