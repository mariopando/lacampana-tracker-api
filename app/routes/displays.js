// Display API route
var display = require('../models/display.js');
var jwt  = require('jsonwebtoken');
var config = require('nconf');


module.exports = function(router) {
    'use strict';

    // events
    router.route('/')
        .get(function(req, res, next) {
            display.find({}, function(err, display) {
                if (err){
                    res.json({ status: 0, payload: err });
                    return next(err);
                }
                else{
                    res.json({ status: 1, payload: display });
                }
            });
        });

    /**
     * Token Verify
     */
    router.use(function(req, res, next) {

        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, config.get('SESSION_SECRET'), function(err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }
    });
};