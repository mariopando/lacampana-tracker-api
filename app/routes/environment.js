// app/routes/users.js
var environment = require('../models/environment.js');
var jwt  = require('jsonwebtoken');

module.exports = function(router) {
    'use strict';

    // events
    router.route('/')
        .get(function(req, res, next) {
            environment.find(function (err, environment) {
                if (err) return next(err);
                res.json({status: 'ok', data: environment});
            });
        });

    /**
     * Token Verify
     */
    router.use(function(req, res, next) {

        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, config.get('SESSION_SECRET'), function(err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }
    });


};